---
layout: post
title: Ultima VIII on Linux
image: "/content/images/2018/01/Auswahl_701.png"
date: '2018-01-21 20:09:03'
tags:
- linux
- games
---

Don't you miss the good old games sometimes? Okay, opinions diverge when it comes to exactly this Ultima. Ultima 8 is indeed much different from the previous Ultima titles in the saga, which reached its peak with [Ultima 7](http://exult.sourceforge.net/ "Check out Excult if you want to play Ultima 7"). Don't get me wrong, the story is still awesome and you can dive into a world with great depth. But with the Avatar walking around alone, all the jumping and the much different fight system Ultima 8 was simply too different for most of the fans.

Nevertheless I wanted to play it again, because I loved it in my youth and it was the first Ultima I've played. So where to get it from? If you've still got a copy in your basement, you are a lucky person. Otherwise you should head over to [GOG.com](https://www.gog.com/game/ultima_8_gold_edition), where you can get the fully patched Gold Edition for $5.99. Don't ask me where to get the not unimportant patches for the original release. I don't know.

## 1. Installation of the GOG version of Ultima 8

Since Ultima 8 is fully supported by [DOSBox](https://www.dosbox.com/), installation shouldn't be that hard. But there is a problem with the Ultima 8 version from GOG. It's coming with a Windows installer. This cannot be started with DOSBox and we have to grab the bag of tricks. Tricks like [Wine](https://www.winehq.org/) in that case. So just install Wine. After that you should be able to right click the exe-file to open it with Wine. A wizard appears. Pick a language and proceed. Now click on _Options_ and don't install to the default location within _drive\_c_.

![](/content/images/2018/01/Auswahl_703.png)

Instead install to the path where you want it to be. The installer now extracts all of the game contents and probably crashes. But that's okay, since we are only interested in the game itself.

## 2. Ultima 8 in DOSBox

![](/content/images/2018/01/Auswahl_704.png)

Now is the time to install and start DOSBox. After that, you need to mount the path to Ultima 8 **in** DOSBox. Otherwise DOSBox won't see it. You can do this like that: `mount c /home/your_user/path_to_the_extracted_game_contents`. After that (you still know DOS, don't you?) you can switch to C: with `c:` and `cd` to the right folder to start `U8.EXE`. That's it. Ultima 8 should start now!

## 3. Additional configurations

In the first place I had no sound. So I started the `INSTALL.EXE` to set the sound and music card to _Sound Baster Pro_ with the following settings.

Sound card: Sound Blaster Pro 16/16ASP - Port: 0x220 - IRQ: 7 - DMA: 1
Music card: Sound Blaster Pro - Port: 0x220

![](/content/images/2018/01/Auswahl_700.png)

Also the mouse sensitivity was not very great. In order to change this, you have to tweak the DOSBox settings a little bit. You can find those in `~/.dosbox/`. Since I've got version 0.74 installed, my config file is here: `~/.dosbox/dosbox-0.74.conf`. Look for `sensitivity`. I had to set it to `50`.

There is a lot more you can do here. Just check the [DOSBox Wiki](https://www.dosbox.com/wiki/) to get some infos.

Have fun with Ultima 8! :)

PS: This little guide should also work for other DOSBox games from GOG that are published for Windows.

![](/content/images/2018/01/Auswahl_701.png)
