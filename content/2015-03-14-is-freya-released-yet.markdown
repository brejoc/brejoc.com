---
layout: post
title: Is Freya released yet?
image: "/content/images/2016/06/Auswahl_546.png"
date: '2015-03-14 15:08:00'
---

Once uppon a time there has been a website called [Is Freya released yet](http://isfreyareleasedyet.com/), that is no longer needed. Why? Because the [Beta 1](http://elementaryos.org/journal/freya-beta-1-available-for-developers-testers) of [elementary OS](http://elementaryos.org) has been released and it looks awesome. So IFRY is no longer needed, but I'd like to publish some numbers here:

**99,783 unique** visitors with **581,546 total requests** in roughly **two month**. Nearly **9,700 visitors a day**. Most of the visitors already run Linux (52,336), but **19,408 visitors used Windows**. I guess the 20,393 visitors with _Unknown_ Operating Systems are using iOS or Android.  
Most people visited IFRY directly. **G+ brought 11,847 visitors** and **vk.com (don't know what it is) 7,436**. Interestingly reddit and fuckyeah-elementaryos.tumblr.com are nearly on par with 5,592 and 4,333 visitors.  
And a whole bunch of people googled for the URL. ;)

![Server Log](/content/images/2016/06/ifry_stats.png)