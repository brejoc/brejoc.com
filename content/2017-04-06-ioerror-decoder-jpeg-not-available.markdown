---
layout: post
title: "IOError - decoder jpeg not available \U0001F41E"
image: "/content/images/2017/04/pedal.jpg"
date: '2017-04-06 09:45:42'
tags:
- python
- django-2
---

This is one of the blog posts, that is adressing the future me but might also be useful for you too. You might have gotten this error because you are using PIL or Pillow in a Python project.

**IOError - decoder jpeg not available** - I've had this error out of the blue and was a bit confused. Nothing in the virutalenv had been changed, dependencies where still in place and I couldn't find anything missing. So I removed the virtualenv, created a new one and installed all eggs again. Still the same error.

It took me serveral minutes to notice the *"Using cached…"* output from `pip`. `pip` is locally caching packages to save bandwith and to speed up package installations. But those packages can be system specifiy if locally compiled. So I cleaned the package cache (`rm -rf ~/.cache/pip` for Linux and `rm -rf ~/Library/Caches/pip` for MacOS) and repeated the installation. Success! 👍
Most propably `libjpegXXX` got an update somewhen in between and the ABI changed, but Pillow/PIL still used the old ABI. Since the cache was cleaned, the packages had to be re-compiled again and the ABI was matching again.