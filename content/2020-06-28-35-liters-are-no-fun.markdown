---
layout: post
title: '35 l/m² of rain are no fun at all!'
date: '2020-06-28 00:00:00'
description: When 35 liters of rain per m² are coming down in around 45 minutes you better have a prefect sewage system. I don’t - and that’s why my garage, the lab and parts of the basement were magically transformed into a swimming pool yesterday.
tags:
- disaster
---

When 35 liters of rain per m² are coming down in around 45 minutes you better have a prefect sewage system. I don’t - and that’s why my garage, the lab and parts of the basement were magically transformed into a swimming pool yesterday. No fun at all! Usually we don’t have that amount of rain in such a short time in my area. Last time I saw that much rain was in Thailand. But I guess we’ll have to get used to heavy rain now, since climate change is slowly but steadily also hitting us here in Europe and we need to get prepared.

So I guess this house needs some modifications. Especially what’s happening with rain water. And of course my driveway shouldn’t turn into a river even during heavy rain. Good that I don’t have anything else to do… :(

The good news is that none of my computer equipment from the lab was affected. Why? I knew that this might happen at some point of time and I was kind of prepared. So there are no cables on the ground. Everything is mounted to the walls or is on top of something. This way there won’t be a short circuit that could destroys something.
All of the computers are either in or (in case of the NAS) on top of the rack. Nice trick isn’t it!? But the rack is also filled from the top and not from the bottom. But that’s not everything: The most important things are top. I know, that’s not rocket science, but when you stick to those easy rules, you can prevent additional thousands of Euros going down the drain.

To summarize it:

1. Put none of your equipment on the ground. Not even the cables.
2. Elevate your computer equipment and don’t forget the switches. A rack makes it pretty easy.
3. The most important things go into the top.

Now here is a special advice. Seal your doors if you have the possibility. I didn’t do that, because I didn’t think about it beforehand. This house has fire protection doors in the lab and the basement. I could have sealed them with simple stuff from the hardware store and less or even no water would have made it into the lab and the basement. Only the garage would haven been flooded. Well, some things you only learn from experience.

