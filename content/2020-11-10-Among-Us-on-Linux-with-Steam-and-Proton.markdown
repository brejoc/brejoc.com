---
layout: post
title: 'Among Us on Linux with Steam and Proton'
date: '2020-11-10 00:00:00'
description: 'Among Us is a fun little game. Unfortunately the Stream store only has a Windows version. But you can still play it on Linux.'
tags:
- Linux
- Games
---

Among Us is a fun little game. Unfortunately the Stream store only has a Windows version. But you can still play it on Linux.

{{< youtube  zG_9Sb8r0_M>}}
[Link to video on YouTube](https://www.youtube.com/watch?v=zG_9Sb8r0_M)
