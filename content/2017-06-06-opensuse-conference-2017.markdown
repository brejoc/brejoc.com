---
layout: post
title: openSUSE Conference 2017 - My Highlights
image: "/content/images/2017/06/osc17_cover.jpg"
date: '2017-06-06 18:45:23'
tags:
- linux
- opensuse
- conference
---

This has been my first [openSUSE Conference](https://events.opensuse.org/conference/oSC17) and it has been great. Thanks to all the organizers and supporters!

## My Highlights

I have to admit, that I lost track of openSUSE and SUSE about ten years ago and I was really surprised to see such a mature and compelling ecosystem. Yes, the ecosystem is the most compelling thing to me. Don't get me wrong, openSUSE and SLES are great Linux distributions, but the ecosystem is what elevates this above all the others!

I'm talking in particular about [Open Build Service (OBS)](https://build.opensuse.org/), [openQA](http://openqa.opensuse.org/), [YaST](http://yast.github.io/) and [Kiwi](http://opensuse.github.io/kiwi/). With this combination you can build packages, create ISO and docker images and test them with openQA.  
YaST, once a pain in the butt, has been revamped completely and is awesome now. Auto-YaST lets you provision servers directly with Salt. You sure could do that with Salt-Cloud too, but having such a thing in YaST makes sense! Oh and Salt is literally everywhere in and around openSUSE. It looks like everybody at openSUSE is switching to Salt when it comes to configuration management and server orchestration.

And then there is [Tumbleweed](https://www.opensuse.org/#Tumbleweed). Tumbleweed is a rolling distribution. But in contrast to other rolling distributions Tumbleweed is heavily tested with openQA which makes it pretty stable. Granted, you still might not want to use Tumbleweed on a production system. *But* you might want to target Tumbleweed for development, because the next stable release will be around the corner and Tumbleweed might help you to catch those nasty compatibility bugs. Testing beta releases of the next stable release is no fun (just like factory builds). At that point you might be struggling with 40 bugs and their interdependencies. Not a great thing to waste time with.  
Wouldn't it instead be better to test your software on the stable release *and* also on Tumbleweed? Then you could fix the bugs as they roll along – one after an other. And when the next stable release is out, you can switch without the usual pain and sweat.

### Videos

* [Moving Beyond Infrastructure as Code](https://www.youtube.com/watch?v=VuwvvbWcT44&list=PL_AMhvchzBacOM0DTf7pn-S_duElE9yMC&index=20)  
* [How I Learned to Stop Worrying and Love Tumbleweed, but Still Occasionally Worry](https://www.youtube.com/watch?v=RbP9lNvmWKk&list=PL_AMhvchzBacOM0DTf7pn-S_duElE9yMC&index=14)  
* [openSUSE & Jurrassic Park](https://www.youtube.com/watch?v=JjnsskKgL6U&list=PL_AMhvchzBacOM0DTf7pn-S_duElE9yMC&index=26)  
* … and as a follow up: [OBS ❤ AppImage](https://speakerdeck.com/probonopd/opensuse-conference-2017-obs-b-appimage)  
* [How openQA works](https://www.youtube.com/watch?v=vGpH4SQ9rQo&index=18&list=PL_AMhvchzBacOM0DTf7pn-S_duElE9yMC)
* [From source to package](https://www.youtube.com/watch?v=aubuZXcyuVA)

* **Special**:  [My Move to SUSE](https://www.youtube.com/watch?v=8xsq_HFaEOA&list=PL_AMhvchzBacOM0DTf7pn-S_duElE9yMC&index=16)  
  I didn't know Thomas Hatch could also work as a standup comedian! ;)  

You can find all the [other videos on YouTube](openSUSE Conference 2017: https://www.youtube.com/playlist?list=PL_AMhvchzBacOM0DTf7pn-S_duElE9yMC). 