---
layout: post
title: Salt and openSUSE
image: "/content/images/2018/06/openSUSE_and_SaltStack.jpg"
date: '2018-06-21 13:31:19'
tags:
- devops
- sysadmin
- linux
- opensuse
- saltstack
---

If you haven't been living under a rock, you've certainly noticed that Salt is one of the hot topics within openSUSE and SUSE. It is used in many of the major openSUSE projects like ~~[DeepSea][gh-deepsea] for managing and automating Ceph deployments~~, in [Kubic][web-kubic] to provision Kubernetes cluster or the newly announced Spacewalk fork [Uyuni project][web-uyuni] to manage your whole IT infrastructure. Therefore I think it's time to take a closer look at how Salt itself is being packaged and shipped and how you might collaborate.

## Packages

Let's talk about the RPM packages itself for a moment. Usually when you install Salt (`zypper in salt`), you don't have to care from which repository the packages are coming. They are either in the Leap or Tumbleweed repos. But before they end up there, they are first in [saltstack:testing/salt][obs-saltstack:testing/salt] (which is for testing the new changes) and then in [saltstack/salt][obs-saltstack/salt]. After the patches are accepted to `saltstack/salt`, the state is pushed to [openSUSE:Factory/salt][obs-openSUSE:Factory/salt]. This is only true for now, because we've got the rare condition where Leap 42.3, Leap 15, Tumbleweed and SLE15 are shipping Salt 2018.3.0. But this will change in the future, because Tumbleweed will move on. Leap 15 and SLE 15 will both stick to the same version, which is 2018.3.0. I know that this was different in the past. But this caused confusion and there is no good reason for having a different version in Leap and SLE.

But don't let the version numbers fool you. We are still pulling in bugfixes and even backport important features if we need them.


## How to get changes in?

This is the easy part. Just do it [upstream][saltstack/salt], and wait for the changes to be approved and merged. Then create a pull request in the [openSUSE/salt GitHub repository][openSUSE/salt] by [cherry picking][docs - git cherry pick] your changes from upstream, and squashing them into a single commit.

You can also do this for upstream changes you think should be in the openSUSE package. I can't guarantee that everything will be accepted. But just try it!

## What is already in?

Previously I've mentioned that even though we are shipping version 2018.3.0 of Salt, we don't just stick to the dot-release. The Salt dot-release is just the base the openSUSE Salt package is build on. That's why there are already 25 patches coming with that package. In the [spec file][saltstack/salt/salt.spec] you can even find the links to the upstream pull requests. This is an easy way to keep track of what got fixed and what you might want to cherry pick from upstream. ;)

Currently the patch list looks like this:

```
Patch1:        run-salt-master-as-dedicated-salt-user.patch
Patch2:        run-salt-api-as-user-salt-bsc-1064520.patch
Patch3:        activate-all-beacons-sources-config-pillar-grains.patch
Patch4:        avoid-excessive-syslogging-by-watchdog-cronjob-58.patch
Patch5:        feat-add-grain-for-all-fqdns.patch
Patch6:        fix-bsc-1065792.patch
# PATCH-FIX_UPSTREAM https://github.com/saltstack/salt/pull/46006
Patch7:        remove-obsolete-unicode-handling-in-pkg.info_install.patch
Patch8:        fix-openscap-push.patch
# PATCH-FIX_UPSTREAM https://github.com/saltstack/salt/pull/45972
Patch9:        move-log_file-option-to-changeable-defaults.patch
# PATCH-FIX_UPSTREAM https://github.com/saltstack/salt/pull/46416
Patch10:       fix-cp.push-empty-file.patch
# PATCH-FIX_UPSTREAM https://github.com/saltstack/salt/pull/46575
Patch11:       fix-decrease-loglevel-when-unable-to-resolve-addr.patch
# PATCH-FIX_UPSTREAM https://github.com/saltstack/salt/pull/46643
Patch12:       make-it-possible-to-use-login-pull-and-push-from-mod.patch
# PATCH-FIX_UPSTREAM https://github.com/saltstack/salt/pull/46413
Patch13:       explore-module.run-response-to-catch-the-result-in-d.patch
# PATCH-FIX_UPSTREAM https://github.com/saltstack/salt/pull/46684
Patch14:       add-saltssh-multi-version-support-across-python-inte.patch
# PATCH-FIX_UPSTREAM https://github.com/saltstack/salt/pull/46635
Patch15:       fix-for-errno-0-resolver-error-0-no-error-bsc-108758.patch
# PATCH-FIX_UPSTREAM https://github.com/saltstack/salt/pull/46890
Patch16:       fall-back-to-pymysql.patch
# PATCH-FIX_UPSTREAM https://github.com/saltstack/salt/pull/47149
Patch17:       strip-trailing-commas-on-linux-user-gecos-fields.patch
# PATCH-FIX_UPSTREAM https://github.com/saltstack/salt/pull/47155
Patch18:       provide-kwargs-to-pkg_resource.parse_targets-require.patch
# PATCH-FIX_UPSTREAM https://github.com/saltstack/salt/pull/47270
Patch19:       initialize-__context__-retcode-for-functions-handled.patch
# PATCH-FIX_UPSTREAM https://github.com/saltstack/salt/pull/47232
Patch20:       fixed-usage-of-ipaddress.patch
# PATCH-FIX_UPSTREAM https://github.com/saltstack/salt/pull/47497
Patch21:       extra-filerefs-include-files-even-if-no-refs-in-stat.patch
# PATCH-FIX_UPSTREAM https://github.com/saltstack/salt/pull/47504
Patch22:       option-to-merge-current-pillar-with-opts-pillar-duri.patch
# PATCH-FIX_UPSTREAM https://github.com/saltstack/salt/pull/47471
Patch23:       do-not-override-jid-on-returners-only-sending-back-t.patch
# PATCH-FIX_OPENSUSE bsc#1091371
Patch24:       enable-passing-a-unix_socket-for-mysql-returners-bsc.patch
# PATCH-FIX_UPSTREAM https://github.com/saltstack/salt/pull/47211
Patch25:       fix-for-ec2-rate-limit-failures.patch
```

I hope you enjoyed this little overview. Just ping me on freenode#suse if you've got questions or suggestions. My nick is `brejoc` and `brejoc[m]`. 

Have fun and happy hacking!

<small>
_Edits:_  
_1. DeepSea is not an openSUSE project. It is driven by SUSE._  
_2. Some rephrasing._  
_3. Added [Matrix/IRC](https://matrix.org/docs/projects/as/irc-bridge.html) nick._  
4. Fixed Salt package name.
</small>

[web-kubic]: https://kubic.opensuse.org/
[gh-deepsea]: https://github.com/SUSE/DeepSea
[web-uyuni]: https://www.uyuni-project.org/
[obs-saltstack:testing/salt]: https://build.opensuse.org/package/show/systemsmanagement:saltstack:testing/salt
[obs-saltstack/salt]: https://build.opensuse.org/package/show/systemsmanagement:saltstack/salt
[obs-openSUSE:Factory/salt]: https://build.opensuse.org/package/show/openSUSE:Factory/salt
[saltstack/salt]: https://github.com/saltstack/salt/
[openSUSE/salt]: https://github.com/openSUSE/salt/
[docs - git cherry pick]: https://git-scm.com/docs/git-cherry-pick
[saltstack/salt/salt.spec]: https://build.opensuse.org/package/view_file/systemsmanagement:saltstack/salt/salt.spec?expand=1
