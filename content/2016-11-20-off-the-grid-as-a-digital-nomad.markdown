---
layout: post
title: Off the Grid as a Digital Nomad
image: "/content/images/2016/11/upload62591473.jpeg"
date: '2016-11-20 20:11:28'
tags:
- digitalnomad
- remote
- imho
---

Being a Digital Nomad has a lot of benefits, but there are also some major downsides. Not having permanent access to the energy grid is one of them. Pluging your laptop or smartphone into the next outlet might not always be possible. Heck, there might not even be an outlet.

I'm in Spain right now and Spain has a surprisingly decent network coverage. But roaming around in a camper often leads you to remote places. Using the 12V batteries of the camper is not yet possible. So right now I have to solely rely on power banks to charge two laptops, two phones, three tablets and two smart watches. At least not every device needs to be fully charged all the time.

My fleet of devices:

* TUXEDO InfinityBook 13,3"
* MacBook Pro 13"
* Nexus 5X
* Moto G3 (my wifes phone)
* Nexus 7
* Nexus 10
* iPad 2
* Pebble Time Round
* Pebble 2

I'd like to get rid of some of them, but that's not yet possible. The most critical devices are the two laptops and the phones. The tablets usually don't need to be fully charged all the time, but it can happen.  
So those are the devices that need to be charged with these two power banks:

* XTPower MP-32000 Powerbank  
  32,000mAh with 2 USB ports up to 2.1A and DC 9V/12V/16V/19V/20V up to 4.5A
* Anker Astro E7 (2nd Gen)  
  26,800mAh with 3 USB ports up to 4A

The XTPower, due to the high capacity and the DC outlet, is mainly used to charge the laptops. The Anker Astro is used for smartphones and tablets. There is one caveat with the XTPower. There are a lot of adapters already included, but one for the MacBook is missing. But if you pay the usual stupid-tax of 20,- EUR, you can get one that fits really well. Charging the MacBook with it is not a problem.  
XTPower once sold an even bigger power bank. I think it was around 50,000mAh. Sadly it was out of stock on Amazon and even XTPower didn't list it in the shop.

So how is it working? The XTPower really is a powerful device. I'm able to fully charge the InfinityBook/MacBook Pro nearly tree times. If I'm not compiling stuff all day long, this gives me enough juice for about three workdays.

The Anker Astro lets me charge the mobile devices up to 5 or 6 times depending on the device mix. I'd say this is a pretty good result.

You might think, that the laptops with the three charges are the limiting factor, but I'm additionally using the Nexus 5X for tethering and that drains the batteries pretty fast. If you also rely on your phone for navigation, you should invest in a USB car adapter to charge the phone while driving aground.

Charging the power banks on the other hand takes some time. To fully charge them, you need to plug them in for about a night. A normal workday in e.g. a coworking space is not enough to get them to 100% if they are empty.

## Conclusions

The two power banks give me enough freedom for approximately two or three days. While this is usually enough, I'm eager to work out an other solution. We've got solar panels on the roof that charge two additional car batteries, which can be used for all kinds of gadgets. The only thing missing are some step down modules to convert the 12V down to e.g. 5V. I guess this is going to be my next target.

I'd also like to mention that I think that the future of mobile computing, even with laptops, is ARM-based. I had the opportunity to test a Chromebook for some time. While the OS is a bit too limited for me, the battery life is phenomenal. You can use those things for days without charging.
