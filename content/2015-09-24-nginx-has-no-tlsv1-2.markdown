---
layout: post
title: Nginx has no TLSv1.2!???
image: "/content/images/2015/09/IMG_20150711_182936.jpg"
date: '2015-09-24 15:16:15'
tags:
- deployment
- devops
- linux
- web
---

Which is of course not true. Setting `ssl_protocols` to `ssl_protocols TLSv1 TLSv1.1 TLSv1.2;` will activate TLSv1, v1.1 and v1.2. So why am I writing this? 

The [documentation](http://nginx.org/en/docs/http/ngx_http_ssl_module.html#ssl_protocols) clearly states that `ssl_protocols` can be set in the context of `http` and `server`. Which is true. But you can set `ssl_protocols` only [once per port](https://trac.nginx.org/nginx/ticket/766). Sadly this is not mentioned in the documentation. I don't know if Nginx stops after the first definition or takes the last definition, but that was exacly  my problem. 
I had a server with multiple site-configurations on one port (aka vhosts on port 80). Since I was testing new configurations, I only enabled TLSv1.2 for one config… and nothing happend; only TLSv1 was possible. So the [search began](http://askubuntu.com/questions/675589/tls-1-2-with-ubuntu-14-04-and-nginx/).

**tl;dr:** With Nginx you can define `ssl_protocols` only once per port. So it's better to keep it in the `nginx.conf` or to create a separate `ssl.conf` with all the ssl parameters you need. Multiple protocols on one port are not possible.

Does anyone know whom to poke for changes in the docs? Please [send me a note](mailto:brejoc@gmail.com).