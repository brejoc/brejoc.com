---
layout: post
title: Mail Comments or How Comments Suck Less
date: '2009-10-12 13:34:00'
---

Recently I've been thinking about an alternative for the widespread way of commenting to blogs or websites in general. Nowadays nearly everyone (including me) has a blog and writes uberimportant stuff to the world. That's nice and fine, but since we have web 2.0 we need to let others comment the stuff we write. This way we get a communication where everyone can participate - and I think that is the really important thing here. But can really everyone participate?

Theoretically the answer would be yes. But let's be honest: Most of the times you read something and you would like to write a comment you press the commenting button and a sign in form pops up. Great - now this is the point where I close the tab and no real communication happened. Just a monologue from the guy with the website. The barrier was just too high. I am not willing to register at every blog or website just to leave a comment. That's nonsense! I bet you know what I am talking about. 

I thought about this a little while and realized it could be much easier, because there is something everybody has, everybody knows how to use it and, most importantly, it works nearly everywhere. What is it? An email account! Yes, that's it. How easy would it be if the blog post would have a unique email address - something like vebadisi34@webgargabe.de and every mail I would write to this address would be added to the list of comments for this blog post. Nice and easy - eh? And with a little magic you could also be informed if someone posts further comments to yours.

But not only the guy that writes tons of comments would be pleased. Also the website owner would have benefits from that system. There are some very good and effective spam filters for mails. Would be a waste not to use such a mature infrastructure. Also you wouldn't have to log the IP address to identify the person behind the comment, you have their email addresses. If someone posts evil stuff you could just write them a mail and remove the comment. Ultimately you could just blacklist the address and no further comment would come through... at least not with that mail address.

Someone willing to code this for me? ;)
