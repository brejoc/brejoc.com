---
layout: post
title: Cup-Recipe For (Django) Python Deployment Part 3 - Deployment
image: "/content/images/2015/01/Auswahl_370-2.png"
date: '2014-01-05 14:47:00'
tags:
- deployment
- devops
- fpm
- python
- ubuntu
- package-management
- linux
- happyadmin
---

### Happy New Year

Happy new year everyone! I hope you could spend a nice time with your family and friends.

I'm very sorry, that the third part took me so long. The end of the year is always a tough time, but this year was especially busy. We had a lot work over at [dajool] and I needed to take some time off during the holidays to relax a bit.

#### Deployment

In [part 2] we looked into the details of the package generation. Now I'd like to give you an idea of how to deploy the packages and even how to automate the deployment.

##### The Easy Way With scp… For Testing

After building the deb package you will want to deploy the package. For a simple test, you can just scp the deb package to the server and directly install the package with `dpkg -i my_package.deb`. However dpkg might complain about missing dependencies. Note that this won't happen, if you are able to install the package with `apt-get` - like when you are installing from your own repository.  Just type `apt-get install -f` to install the missing dependencies. 


##### Full Blown Deployment With reprepro

Manually copying deb file to multiple servers is not very convenient and no time saver at all. This is where reprepro comes into play. reprepro is a Debian package repository producer. This means reprepro just builds the folder and file structure needed for a repository, which might look like this:


```bash
.
├── conf
│   ├── distributions
│   └── options
├── db
│   ├── checksums.db
│   ├── contents.cache.db
│   ├── packages.db
│   ├── references.db
│   ├── release.caches.db
│   └── version
├── dists
│   ├── precise
│   ├── quantal
│   └── raring
├── incoming
├── keys
│   └── public1.key
└── pool
    ├── contrib
    └── main
```

An installation and configuration howto can be found in [this wiki article from debian.org][install_reprepro]. It basically tells you how the configs should look like and how the GPG-key can be generated. Generating a GPG-key is highly recommended if you are really going to use this setup. Otherwise apt-get won't be able to verify the origin and authenticity of the deb file and will complain. If you prefer Nginx over Apache you can use [this very simple, uncomplete but sufficient sample config][nginx_sample_config].

Your distribution file might look like this:

```
Origin: neverland
Label: neverland - precise
Suite: stable
Codename: precise
Architectures: i386 amd64 powerpc
SignWith: 7B8F4ACB
Components: main non-free contrib
Description: Ubuntu repository for precise packages from neverland.
```

The 'Codename' should always be conform to the codename used in the `sources.list` - otherwise this will be a nice source of errors for anyone else but you. If you'd like to add more distributions, just copy this section and make the distribution specific modifications.

After reprepro knows which distributions and architectures you'll want to host, you can add new deb packages to your repository like this: `reprepro -b <path_to_repo> includedeb <codename> <path_to_deb>`

The package is immediately available and can be installed with `apt-get update && apt-get install my_django_app` – at least if you added the repository to your `/etc/apt/sources.list`. To add this repository to your `sources.list` you'll just have to append the line `deb http://my_neverland_domain.com precise main`.

That's it. Now everyone on the planet can `apt-get install` packages from your server. `apt-get` also supports https and basic authentication - just in case you want to host more private deb files. You'll just have to adjust your Apache or Nginx config.

RPM users can use `createrepo` explained in [this blog post][createrepo].


##### Automated Deployment With SaltStack

If you are not familiar with [SaltStack][saltstack]; It's an infrastructure management tool to orchestrate your servers (aka minions) from your master. Salt can do everything: You can execute commands on the minions, do things periodically, distribute config files or install packages. You should especially take a look at the [grains][salt_grains]. With the information SaltStack collects from the system - the grains - you are able to target minions from a certain group/role or location, with a specific OS, but also minions with e.g. four CPU or just the minions with a specific GPU if you are doing some CUDA number crunching.

A very nice [installation instruction][install_salt] can be found in the excellent documentation.

Now you can do multiple things. Let's assume that you are using [Jenkins][jenkins] as a build server. You trigger builds either from an [incoming hook][hg_hooks] in your central mercurial repository or just let Jenkins check the mercurial repository periodically for changes. Git uses a [very similar system][git_hooks]. The deb files (artifacts called in Jenkins) then get pushed into reprepro by the salt master, which updates the package on your test server afterwards for QA. 
It would also be possible to not use Jenkins at all and manage the builds from the salt master. But then you would have to write some more glue code. You could also use a specific branch in your mercurial repository or tags for production builds. The possibilities are endless.

Nice, isn't it? Since this topic ist so manifold and interesting I would like to start an experiment here. I've created a mailing list on [librelist.com][librelist]. What are your thoughts on deployment with packages? Do you have questions, annotations or do you just want to discuss a part of the setup? Just send an e-mail to [happyadmin@librelist.com][happyadmin] and you'll be subscribed to the list. Any e-mail send afterwards will be posted to the list.



[happyadmin]: mailto:happyadmin@librelist.com "happyadmin@librelist.com"

[librelist]: http://librelist.com/ "Librelist is a free as in freedom mailing list site for open source projects."

[part 2]: http://brejoc.com/cup-recipe-for-django-python-deployment-part-2-detailed-overview/ "Cup-Recipe For (Django) Python Deployment Part 2 - Detailed Overview"

[install_salt]: http://docs.saltstack.com/topics/installation/ubuntu.html "Ubuntu installation."

[hg_hooks]: http://mercurial.selenic.com/wiki/Hook#Tips_for_centralized_repositories "Basic information about hooks - Tips for centralized repositories"

[git_hooks]: http://git-scm.com/book/en/Customizing-Git-Git-Hooks#Server-Side-Hooks "Git-Hooks | Server-Side Hooks"

[jenkins]: http://jenkins-ci.org/ "An extendable open source continuous integration server"

[salt_grains]: http://docs.saltstack.com/topics/targeting/grains.html "Salt comes with an interface to derive information about the underlying system. This is called the grains interface, because it presents salt with grains of information."

[createrepo]: http://www.techrepublic.com/blog/linux-and-open-source/create-your-own-yum-repository/609/ "Create your own yum repository"

[nginx_sample_config]: http://paste.ubuntu.com/6692630/ "Very simple Nginx sample config."

[install_reprepro]: https://wiki.debian.org/SettingUpSignedAptRepositoryWithReprepro "Setting Up Signed Apt Repository With Reprepro"

[dajool]: http://dajool.com "Sorry, only german content at the moment."

[me_gplus]: https://plus.google.com/117203070375063446694/posts "Jochen Breuer on G+"

[mercurial]: http://mercurial.selenic.com/ "Mercurial is a free, distributed source control management tool. It efficiently handles projects of any size and offers an easy and intuitive interface."

[lxc_ubuntu]: https://help.ubuntu.com/community/LXC "LinuX Containers (LXC) provide lightweight virtualization…"

[fpm]: https://github.com/jordansissel/fpm/ "Effing Package Management"

[reprepro]: http://mirrorer.alioth.debian.org/ "reprepro"

[invoke]: http://docs.pyinvoke.org/en/latest/ "Invoke is a Python (2.6+ and 3.2+) task execution tool & library, drawing inspiration from various sources to arrive at a powerful & clean feature set."

[fpm_pkgng]: https://github.com/jordansissel/fpm/issues/62 "fpm should be able to build freebsd packages!"

[ubuntu]: http://www.ubuntu.com/server "Ubuntu Server — for scale out computing"

[debian]: http://www.debian.org/ "Debian -- The Universal Operating System"

[rhodecode]: https://rhodecode.com/

[saltstack]: http://saltstack.com/community.html "SaltStack takes a new approach to infrastructure management by developing software that is easy enough to get running in minutes, scalable enough to manage tens of thousands of servers, and fast enough to communicate with them in seconds."

[cheese_shop]: https://pypi.python.org/pypi "PyPI - the Python Package Index"

[rubygems]: http://rubygems.org/ "Find your gems easier, publish them faster, and have fun doing it."

[jinja]: http://jinja.pocoo.org/docs/ "Jinja2 is a full featured template engine for Python. It has full unicode support, an optional integrated sandboxed execution environment, widely used and BSD licensed."

[IDS]: http://en.wikipedia.org/wiki/Intrusion_detection_system "Intrusion detection system"