---
layout: post
title: SuperGenPass implemented in Go
image: "/content/images/2015/08/IMG_20150710_085927.jpg"
date: '2015-08-02 11:35:35'
tags:
- package-management
- linux
- golang
- osx
---

Last year I've [started a port](http://paste.dajool.com/p3hfjmoor/zvimni/raw) of SuperGenPass in Go. After my initial release [Mathias Gumz](https://github.com/mgumz) totally dominate the project and [made huge improvements](https://github.com/brejoc/gosgp/commits/master). gosgp was born – a command-line application to generate passwords for domains (or any other string you enter).

Since I'm still using SuperGenPass for many Passwords, gosgp is something I install frequently on my Linux boxes. Recently a new Macbook found its way into my bag and I had to compile gosgp again for OS X. Not a problem at all, but I got reminded of the homebrew [tap](https://github.com/brejoc/homebrew-tap/) for [ssht](https://github.com/brejoc/ssht) and decided to throw together a [recipe](https://github.com/brejoc/homebrew-tap/blob/master/gosgp.rb).

So, it is now possible to install gosgp with two easy commands:

    $ brew tap brejoc/tap
    $ brew install gosgp

**Update:**  
I've just realized, that I haven't published the Makefile I'm using to build Linux packages. Well, [here it is](https://github.com/brejoc/gosgp/blob/master/Makefile)!

You can just clone the [git repo](https://github.com/brejoc/gosgp/), [install fpm](https://github.com/jordansissel/fpm#get-with-the-download) and [Go](https://golang.org/doc/install) and enter `make package_deb` or `make package_rpm`. You'll directly get a shiny new Debian or RPM package.