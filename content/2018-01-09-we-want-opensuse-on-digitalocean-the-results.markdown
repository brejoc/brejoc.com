---
layout: post
title: We want openSUSE on DigitalOcean - The results
image: "/content/images/2018/01/IMG_20161230_130651_small-1.jpg"
date: '2018-01-09 19:12:42'
tags:
- deployment
- devops
- linux
- opensuse
---

Two month ago I started this [survey](https://docs.google.com/forms/d/e/1FAIpQLSfORqAPZyf7_kXJaAaHJQtH5OLyywX16q0xYDOrcdRSIUSQwA/viewform) and the results are finally here. Many thanks to all who took the time to send this in.

All of this is based on 66 answers. All of the answers where mandatory with one possible answer, besides the last two that where multiple choice and completely optional.

### Are you already a DO customer?

![](/content/images/2018/01/output_1_1.png)

### Are you an openSUSE user?

![](/content/images/2018/01/output_2_1.png)

### Are you using other cloud hosting providers because of their openSUSE support?

![](/content/images/2018/01/output_3_1.png)

### Would you be interested in openSUSE on DigitalOcean?

![](/content/images/2018/01/output_4_1.png)

Luckily noone answered with 'no' here! :)

### How many openSUSE Droplets would you probably spin up?

![](/content/images/2018/01/output_5_1.png)

Sorry for the overlapping answers. But I guess you got it anyways.

### Which Droplet sizes would you primarily need?

![](/content/images/2018/01/output_6_1.png)

### Which additional service would you also be interested in?

![](/content/images/2018/01/output_7_1.png)

### Some comments
And last but not least some comments people where sending in.


> I'd love to be able to spin up OBS worker appliances (using openSUSE) on demand in DigitalOcean.

&nbsp; 

> The main reason for wanting OpenSUSE Leap is because the other Linux distros have some drawbacks for me: Debian+CentOS have packages that are generally too old for me. Fedora is too bleeding-edge (upgrades every 6 months too). Ubuntu is alright, but they have dropped the ball with handling CVEs (sometimes MONTHS behind Debian) for packages I have used. OpenSUSE Leap would be nice because it has a nice mix of having up-to-date packages, stability, and staying on top of CVEs.

&nbsp;

> I host game servers for my friends and I. I would really like to use openSUSE for the servers instead of Ubuntu and CentOS. 

&nbsp;

> SUSE rocks!

Thanks again and happy hacking!