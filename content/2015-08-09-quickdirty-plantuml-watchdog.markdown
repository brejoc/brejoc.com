---
layout: post
title: 'Quick&Dirty: PlantUML watchdog'
image: "/content/images/2015/08/IMG_20150714_134623.jpg"
date: '2015-08-09 18:06:35'
tags:
- linux
- elementaryos
- osx
- quickndirty
- hack
---

We've been using [PlantUML](http://plantuml.com/) in the past to outline dependencies and relations between classes or to simply get the relations in databases right. There is a nice Confluence plugin and serveral editors availabe, that let you edit your PlantUML file in the browser and show the generated graph image right beside it. That is neat, but I prefere my own computer and I like vim. But re-running the command to generate the image every 10 seconds manually is tedious.

So, here is a quick and dirty hack that generates a new image for your .plantuml file whenever it has been created or changed. Just start the watchdog in the folder you want to work in and `plantwatchdog.py` takes care of the rest. Don't forget to take a look at the dependencies first. You'll need [sh](https://pypi.python.org/pypi/sh/), [watchdog](https://pypi.python.org/pypi/watchdog) and of course the [plantuml.jar](http://sourceforge.net/projects/plantuml/files/plantuml.jar/download). Yeah, that also means you'll need to have a JRE. You'll also need to create a shell script file named `plantumlpipe` with the correct path to your `plantuml.jar` somewhere in `$PATH`:

```
#! /usr/bin/env sh
java -Djava.awt.headless=true -jar /<the_path_to>/plantuml.jar -pipe
```

<script src="https://gist.github.com/brejoc/0c07d091af31cbcf9812.js"></script>