---
layout: post
title: CI with drone.io and your own images
image: "/content/images/2015/11/IMG_20151109_142631.jpg"
date: '2015-11-05 11:52:15'
tags:
- deployment
- devops
- linux
- continuous-integration
- testing
---

I've been evaluating [drone.io](http://drone.io) yesterday and I'm quite impressed with the integration and simplicity. You can use drone as a SAAS or [install it on your own hardware](http://readme.drone.io/setup/install/ubuntu/). It's really easy. Drone comes with batteries included. After setting up OAuth with your repository system (e.g. [Gitlab](http://readme.drone.io/setup/config/gitlab/) in our case), you are good to go. You can just select the project from a list and then you are asked to place a `.drone.yml` in the project root - which could look like this:



```yaml
image: brejoc/trusty
script:
  - make
  - make test
notify:
  email:
    recipients:
      - me@example.com
```

With every commit drone now uses the docker image `brejoc/trusty` to build and test the project like defined in the script section. In theory you are able to use any docker image out there. But there seems to be a caveat. You can use any docker image with **Git** installed. Quite a number of images don't ship Git preinstalled, so you'll have to take care of that yourself.

Let's use the Ubuntu image of `trusty` for example:

```Dockerfile
FROM ubuntu:14.04
MAINTAINER Peter Parker "me@example.com"
RUN apt-get -y update
RUN apt-get install -y git
CMD []
```
This `Dockerfile` is enough to get an image with Git and it's enough to make drone happy.   
Now you've got two choices. You can either put this up on [Docker Hub](https://hub.docker.com) so that you can pull the image from anywhere you want, or you can just [build the image on your build server](http://docs.docker.com/engine/userguide/dockerimages/) like this: `docker build -t 'brejoc/trusty' .`

Of couse you should use an other tag. ;) Both ways work flawlessly and drone doesn't care where the image came from.

drone is open source and licensed unter the Apache License Version 2.0. For public projects the SAAS offering is free. There are also payed plans available which include more projects or increased concurrent builds. The website doesn't mention if it is possible to support the project apart from the SAAS plans.