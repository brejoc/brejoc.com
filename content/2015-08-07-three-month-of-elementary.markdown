---
layout: post
title: Three month of elementary OS
image: "/content/images/2015/08/IMG_20150521_120013-2-1-1_small.jpg"
date: '2015-08-07 22:02:28'
tags:
- ubuntu
- linux
- elementaryos
- imho
---

[Three month ago](https://twitter.com/mr_brejoc/status/597129590109315073) I've been installing [elementary OS](https://elementary.io/de/) on my Dell XPS 13. The sputnik edition. The XPS 13 is my main workstation that follows me everywhere I go – so this is more or less like a full switch to elementary for me.
Previously I've been using Ubungu GNOME on the XPS, which is an official flavour of Ubuntu, featuring the GNOME desktop environment. But I'm also using Kubuntu from time to time and vanilla Ubuntu with Unity nearly every day on an iMac. There is also a MacBook with OS X that is used for iOS and Mac related stuff. One could say I'm trying to stay on top of things when it comes to desktop environments. ;)

An HP Workstation I'm using in my home office was already running elementary OS Freya (and previously Luna) since some of the first alphas. So I was quite aware of what I had to expect. But there is a huge difference in using an OS (and desktop environment) from time to time and using it every day to get things done.

## What am I doing with elementary?
 
Since I'm CEO and Head of Cloud Development at [dajool](http://dajool.com), I'm wearing a lot of hats during the day. My field of operation involves web and mobile development, system administration, dev-ops, marketing strategics, strategic planning, aso. Most of my work is done on the shell, in the browser or email client. Therefore my desktop is pretty much standard and most of the customizations I need are under the hood. I'm using the awesome [fish shell](https://github.com/fish-shell/fish-shell) instead of bash. [tmux](http://tmux.sourceforge.net/) is also mandatory and not missing on any system I own or administer. I also operate my own private debian package repositories with lots of more recent or missing packages I maintain myself.

## The great things

### Speed and simplicity

elementary OS is very clean, minmal and gets out of the way, but at the same time (most of the time) manages the balancing act to provide an interface with just enough features. The first thing you'll notice is the speed of elementary. I'd argue that it actually is the fastest GTK based desktop out there.

### The most awesome application starter aka Slingshot

In Linux-world there is not just one application starter and Slingshot might not be the right one for you. But for me Slingshot is one of the reasons why elementary makes a very good impression right from the start.
Slingshot not only starts applications, Slingshot throws them in your face. It is incredibly fast - just like you would expect it from an application starter. Since a few releases Slingshot also lets you calculate. Very handy!

### Workspace management

The workspace management is - very similar to GNOME 3 - dynamic. So there is always a new desktop to move windows to. That alone is a great feature and shouldn't be missing on any desktop. Moving windows between desktops is very fast, convinient und straight forward. At least if you are using keyboard shortcuts. Using the mouse can be a bit tricky. Switching between applications is okay, but should be improved. Later more on that.

### Keyboard shortcuts

This is something where I'm getting very picky. If most of the OS can't be controlled with a keyboard, I'm not interested. elementary is doing a pretty good job in that area. Nearly everything works like you'd expect it to be. Most of the shortcuts based on the command or super keys are used to manipulate windows or workspaces. elementary even tries to [solve the copy & paste problem](http://lmelinux.net/2014/12/06/ctrlc-will-copy-text-clipboard-elementarys-terminal/) of the shell. Well-intentioned, but [breaking vim](http://stackoverflow.com/a/31098351) is never a good idea! Nevertheless I like the idea of having a global shortcut system, that also works on the shell. One thing Apple did absolutely right by the way. But an alternate based shortcus system (just like BeOS had it and [Haiku](https://www.haiku-os.org/) has it now) might be better for the standard PC keyboard layout we are using. But I guess I'm very alone with that opinion.

### elementary is beautiful by default

You can see that the designers and developers not only care about functionality, they also care about esthetics. A domain that was for some years reserved for Apple. Everything looks very polished and smooth, with a deep love for detail. One example? The top panel - it is transparent with some backgrounds and becomes opaque with other backgrounds, so that you can always clearly see what the top panel hosts. This is not only a graphical gimmick, it has also a very real and practical purpose.
The GTK-Theme and icons just fit perfectly and don't get boring or start to annoy after a certain amount of time.

Oh, and elementary ships with the most beautiful set of wallpapers. [This one](http://i.imgur.com/ahXO5Jc.jpg) is the default.

## The bad things

Come on, not everything can be good. There is always some room for improvement. ;)

While the dynamic workspace management is a step into the right direction, there are some loose ends. There are actually two different exposé like features. The workspace switcher also shows all the windows on the current workspace. Something that also can be accomplished with `super`+`w`. All windows from all workspaces can be seen with `super`+`a`. Everything cool so far. But it is only possible to switch windows with the cursor keys in the workspace switcher view. The other views simply don't offer that. I'm (still) not sure how a solution could look like to make more sense. But having two so similar views without similar functionality surely isn't the best solution.

Moving windows between workspaces with the mouse is somewhat inconvenient. Moving the window more that one workspace just takes too long and is complicated. This is a stark contrast to moving windows via shortcuts. It would be more efficient to be able to just grab the icons under the workspace and move windows from there. I might be totally wrong here, but I thought this worked in Luna.

We are still not finished with the workspaces. If you are using an external monitor, you'll pretty soon notice, that the workspace overview won't adjust to the size of the monitor if it was created before the monitor was attached or detached. This goes both ways. So you'll have a very broken workspace overview when you are coming back from your huge external display to your 13" none high dpi display.

Done with workspaces. There is only one thing left to complain about: Pantheon terminal has no themes. The default theme for the terminal is gorgeous. I like it very, very much! But sometimes I'm working outside. Having a dark themed terminal on a glossy display outside in the sun is a dealbreaker. You can't read anything at all! There has to be a second bright theme to choose from. This theme might not be as classy as the dark one, but it will work outside - and in this case I perfere to have somthing not so nice but functional.

# My verdict

elementary managed to hit a sweet spot. It is fast, lightweight and beautiful without sacrificing anything you might expect from a modern operating system. The creators even managed to bring new concepts to the table. Notably [Contractor](http://www.omgubuntu.co.uk/2011/03/contractor-brings-seamless-file-sharing-between-apps-to-the-linux-desktop), that reminds me of Android's intents. Something that makes perfect sense for a modern operating system where data has to be shared between applications.

If you keep in mind that the project is rather young, the [team not very big and the funding very basic](https://elementary.io/de/get-involved) it is mind-boggling how great elementary Freya turned out and how stable it is. I'm using it daily to get stuff done and nothing crashes or has to be restarted on a regular basis.

But for me elementary is not only great for desktop usage, it is also a perfect workstation OS. I can use **all** the packages from Ubuntu, virtualize other operating systems, work with container technologies and try ZFS. And that is what I need and expect!

**Update:**  
I didn't know that the workspace bug regarding external monitors hasn't been reported yet. [But here it is now](https://bugs.launchpad.net/elementaryos/+bug/1483181).
