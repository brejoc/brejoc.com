---
layout: post
title: Two seconds, then kill it with fire!
image: "/content/images/2016/07/cover.jpg"
date: '2016-07-08 07:03:44'
tags:
- python
---

In this example I'm setting a time limit for code execution. If the execution takes too long, the `TimeoutException` is beeing raised and we can proceed with the rest of the program.


<script src="https://gist.github.com/brejoc/4cf2b224b94089ff7817.js"></script>