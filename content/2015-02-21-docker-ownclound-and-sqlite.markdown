---
layout: post
title: docker, OwnCloud and Sqlite
image: "/content/images/2015/02/1000px-OwnCloud2-Logo-svg.png"
date: '2015-02-21 19:52:33'
tags:
- deployment
- devops
- owncloud
- docker
---

I've been playing more with docker recently and decided to deploy OwnCloud with it. Turns out it is not so easy to finde a Dockerfile for OwnCloud that actually works flawlessly. One thing that strikes me, is the fact that most people tend to forget the config folder. Once you want to update your OwnCloud container, you really need the config from that folder. Otherwise your database config is lost and you'll have to reconfigure everything.

So check out [brejoc/docker-owncloud](https://github.com/brejoc/docker-owncloud), which is a fork from [this repository](https://github.com/btobolaski/docker-owncloud) but with a newer OwnCloud (currently 7.0.4), Sqlite support **and** a volume definition for the config folder, so that you can mount it locally or in your data-volume container.