---
layout: post
title: Turn the Bootstrap carousel with FancyBox
image: "/content/images/2015/05/IMG_20150521_120013-2-1.jpg"
date: '2015-05-24 18:33:18'
tags:
- web
- javascript
- bootstrap
- fancybox
---

This is more or less a reminder for me. With this little gist, the Bootstrap carousel and FancyBox2 are working hand in hand when it comes to browsing images. Normally those two don't care for each other – but it would be much nicer, if the carousel would display the next set of images, when one of them was loaded in the FancyBox modal. Well, these few lines of code make it possible:

<script src="https://gist.github.com/brejoc/5d7c53812291063131bd.js"></script>