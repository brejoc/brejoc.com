---
layout: post
title: '"Dynamic web pages without Javascript" by Tim Bell'
image: "/content/images/2018/09/laptop_on_desk.jpg"
date: '2018-09-05 06:54:42'
---

Yesterday I've noticed a great presentation by [Tim Bell](https://twitter.com/timb07) with the title [Dynamic web pages without Javascript](https://2018.pycon-au.org/talks/45351-dynamic-web-pages-without-javascript/), that was given at PyCon AU 2018. He gives an introduction to creating dynamic web applications with [intercooler.js](http://intercoolerjs.org/) and even mentiones [django-intercoolerjs](https://pypi.org/project/django-intercoolerjs/), my little Django wrapper, at some point. Go check it out if you are interested in creating dynamic web applications without fiddeling with JavaScript too much.

<iframe width="763" height="429" src="https://www.youtube.com/embed/eEVRapHQFKI" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>