---
layout: post
title: ssht moved to OBS
image: "/content/images/2017/11/IMG_20161230_130625_small.jpg"
date: '2017-11-01 20:06:50'
---

Yesterday I've set up an [OBS repository](https://build.opensuse.org/package/show/home:brejoc/ssht) for [ssht](https://github.com/brejoc/ssht "A shortcut right into your tmux sessionvia ssh"). ssht allows you to directly connect to a running tmux session on a remote host via ssh. So if you are using openSUSE (Leap or Tumbleweed), Fedora, CentOS or RedHat you can finally make use this instead of building the package yourself. The easiest way is to just [search for ssht at `software.opensuse.org`](https://software.opensuse.org/package/ssht?search_term=ssht). Then you can pick the disto you've installed and choose to download the package directly or you can add the repo with the 1-click installer. Couldn't get easier!

Have fun and happy hacking!