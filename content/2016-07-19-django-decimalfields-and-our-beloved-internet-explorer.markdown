---
layout: post
title: Django, Localized Decimal-Fields, and our beloved Internet Explorer
image: "/content/images/2016/07/keyboard_small.jpg"
date: '2016-07-19 16:21:02'
tags:
- python
- web
- django-2
---

Current versions of Internet Explorer aren't as bad as it used to be. Some even claim that [Safari is the new IE](https://nolanlawson.com/2015/06/30/safari-is-the-new-ie/). We usually don't have to optimize for a particular browser very often. If it works in Firefox and Chrome, the others are just fine. But one thing that didn't work for one of our customers (yes, he sadly uses Internet Explorer exclusively) was the localization of `DecimalField`. So instead of comma, which is the decimal separator in Germany and many other countries, IE would only allow a dot.

If you are using a `ModelForm` instead of a regular `Form`, you can't just pass [`localize=True`](https://docs.djangoproject.com/ja/1.9/topics/i18n/formatting/#locale-aware-input-in-forms) in the field definition. But there is also an solution for this:

````
from django.forms import ModelForm

class MyModelEditForm(ModelForm):
    """ModelForm of MyModel."""
    class Meta:
        model = MyModel
        localized_fields = ('hourly_rate', 'an_other_decimal_field')
```

Instead of the fields (`hourly_rate` and `an_other_decimal_field` in this example) you could also use the special value `__all__` to let Django mark all fields as localized.  
Take a [look at the docs](https://docs.djangoproject.com/en/1.9/topics/forms/modelforms/#enabling-localization-of-fields) for more information. This seems to work down to Django 1.6.x.