---
layout: post
title: Juno with WSGI
date: '2009-11-10 11:37:00'
tags:
- deployment
- python
- web
- juno
---

Just a quick note on the wonderful [Juno](http://github.com/breily/juno) framework, which I've been using for a small project. Sadly the documentation isn't quite clear when it comes to deployment with mod_wsgi. At least I had some head scratching while reading through the documentation and had to figure out how to do it.

This is what you find in the documentation:

    ** WSGI Notes **
    
    Since mod_wsgi requires a function named 'application', you would need to put Juno
    in 'wsgi' mode >and call run() like so: 
    
    config('mode', 'wsgi')  
    application = run()
    
    Those functions will make more sense later.

Perhaps it's just me, but that didn't make much sense to me. So I basically tried some stuff, until I had a working solution. I'm using the config scripts from [django_hoster](http://bitbucket.org/brejoc/django_hoster/) as a basis. You might want to take a look at the [templates](http://bitbucket.org/brejoc/django_hoster/src/tip/templates/).

Basically the only thing you will have to change in comparison to a Django WSGI deployment is the app.wsgi. From here you will have to start the Juno app for the WSGI:

    #!/usr/bin/python  
    import sys, os  
    sys.stdout = sys.stderr  
    # Here my Juno script is located
    sys.path.insert(0, os.path.join(os.path.dirname(\_\_file\_\_), '..\/lib\/'))
     
    import web                   # This is my juno script  
    application = web.run()      # Start the Juno app 

And that's basically all the magic you need to deploy your Juno web app with mod_wsgi. Have fun!
