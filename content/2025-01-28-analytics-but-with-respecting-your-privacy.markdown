---
title: "Analytics, but with respecting your privacy"
date: 2025-01-28T05:46:06+01:00
draft: false
---

New year new blog post. 😉

When it comes to running websites, providing a seamless and respectful user experience is essential. Using privacy-friendly analytics is a great way to achieve this without the hassle of intrusive cookie banners. In this post, I’ll share how I use [Plausible Analytics](https://plausible.io) to gather valuable insights for my two websites—[brejoc.com](https://brejoc.com) and [jochenbreuer.photography](https://jochenbreuer.photography)—while keeping visitors’ privacy intact.

Traditional analytics tools like Google Analytics rely on cookies to track user behavior. While this can provide in-depth data, it also requires compliance with privacy laws such as GDPR, which often leads to those disruptive cookie banners. To avoid those pesky things, and still understand my websites' performance, I turned to Plausible Analytics. Plausible is a lightweight, privacy-focused analytics tool that doesn't use cookies, doesn't collect personal data, and complies with GDPR, CCPA, and other privacy regulations by design. This means I can track website metrics without requiring intrusive consent pop-ups.

[brejoc.com](https://brejoc.com) is built with Hugo, a static site generator that outputs pure HTML files. Hugo’s simplicity makes it fast and secure. Adding Plausible to a Hugo site is straightforward: I copied the tracking script from Plausible’s dashboard and added it to the `<head>` section by editing the head.html file in my Hugo theme. After redeploying the site, I could start tracking visitors instantly.

On the other hand, my photography website, [jochenbreuer.photography](https://jochenbreuer.photography), runs on Ghost, a modern content management system designed with privacy in mind. Ghost doesn't store cookies for regular visitors unless specific features like memberships are enabled. Integrating Plausible into Ghost was simple: I pasted the tracking script into the "Site Header" field in Ghost’s admin panel under the Code Injection settings. With that, my analytics setup was complete.

Using Plausible Analytics helps me balance gaining insights into my websites’ performance with respecting visitors’ privacy. It eliminates the need for annoying cookie banners, complies with privacy regulations, and ensures my websites remain fast and efficient. Whether you’re running a static site with Hugo or a dynamic platform like Ghost, Plausible’s privacy-first approach is an excellent solution for tracking performance without compromising user trust.

If you’re looking for a way to analyze your website while prioritizing privacy, I highly recommend giving Plausible a try. It’s a straightforward, effective tool that works seamlessly across different platforms.