---
layout: post
title: ssht - a shortcut right into your tmux session
image: "/content/images/2015/11/IMG_20150714_134623.jpg"
date: '2015-11-12 19:38:22'
tags:
- sysadmin
- linux
- osx
- quickndirty
---

`ssht` is not exactly new, but I've never blogged about it and it's a really handy timesaver. If you are working on the terminal a lot, there is a big chance, that you are using [tmux](https://tmux.github.io/). tmux allows you to have multiple windows and panes in one shell - locally and remote.

Now `ssht` allows you to ssh to a remote computer and directly connect to a running `tmux` session. If there is no `tmux` session a new session is created. That's it.

### Installation

#### Linux

You can either copy the [tmux sh script](https://github.com/brejoc/ssht/blob/master/ssht) to your `$PATH` or create your own package with [fpm](https://github.com/jordansissel/fpm). You can find a [`Makefile`](https://github.com/brejoc/ssht/blob/master/Makefile) in the [GitHub repository](https://github.com/brejoc/ssht/). Dependent on your packaging system you just have to enter `make package_deb` or `make package_rpm` and then install the generated package.

#### OS X

I'm offering a homebrew tap to install `ssht`:

`brew tap brejoc/tap && brew install ssht`