---
layout: post
title: Batteries are evil
image: https://brejoc.com/content/images/2015/05/IMG_20150526_124903-1.jpg
date: '2018-07-13 10:49:04'
tags:
- batteries
- hardware
- fire
- suse
- hackweek
---

I had an expanding battery some days ago up to the point where the battery deformed the frame of the laptop. Dell support was really stellar and replaced the battery the next day. One of the benefits, if you are working for SUSE.
But I started to digg into the topic and now I'm convinced to buy a new fire extinguisher. Why? Well, if a lithium-ion battery starts to burn, the fire doesn't need oxygen. Everything the fire needs is coming from the battery itself. If the laptop is still plugged in, we've got three kinds of fires. An electrical, a chemical and a metal fire. So you can't choke the fire. Pouring water on the fire only makes things worse. Burning materials might fly into the room, the fire might re-ignite again and on top you might risk getting electrocuted. Apparently there is only one reasonable thing you can do and this is buying a Class D Copper Powder Extinguisher. Yes, they aren't cheap, but a burnt down house would have been worth the money. The only other thing you can do, if you've no extinguisher around, is using dry sand. Let's move to the beach! 😎

Oh, and here is a short motivational video:
<iframe width="560" height="315" src="https://www.youtube.com/embed/2Z9190ruyIM?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>