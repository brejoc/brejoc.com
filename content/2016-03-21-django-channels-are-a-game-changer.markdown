---
layout: post
title: Django Channels - a game changer
image: "/content/images/2016/03/game-changer3.jpg"
date: '2016-03-21 21:10:18'
tags:
- python
- web
- django-2
---

Do you remember the good old days, when everything was stateless and there was, at least most of the times, a response for every request? Those times are gone. Nowadays we want rich, interactive web applications, that not only respond instantly without reloading the entire website, but also push changes directly to the browser and send notifications.

Lets admit it, Django started to look old and busted. If you wanted to use [WebSockets](http://caniuse.com/#feat=websockets) e.g, you had to use some 3rd party eggs that where more or less well integrated and in some cases already abandoned. I know, the grass is always greener on the other side, but Rails is a serious competitor when it comes to implementing [compelling features](https://www.amberbit.com/blog/2015/4/22/why-rails-5-turbolinks-3-action-cable-matter-and-why-dhh-was-right-all-along/) for web developers. But at least Django has [Channels](http://channels.readthedocs.org/en/latest/) now and I'm really excited about it! Why? Because the demand is already there!

Some time ago one of our customers asked us to create a (web) terminal where staff could track their working times. Authentication should be done via an [electronic key system from Euchner](http://www.euchner.de/en-us/Products/Electronic-Key-System-EKS). Basically an NFC reader with Ethernet interface. So [Oliver](http://dajool.com/#team) reverse engineered parts of the protocol and [re-implemented it in python](https://github.com/olisim/eks). Reading the key in the EKS was no longer a problem. But letting the terminal know which user just plugged in a key screamed for WebSockets. It took quite some time (and testing) until we settled with a [very minimal WebSocket implementation](https://pypi.python.org/pypi/websocket-server). Something we could, if we ever really needed to, maintain ourselves. Don't get me wrong, everything works rock solid, but don't just assume you get stuff like authentication or routing for free with such a leightweight implementation. You have to write a lot of boilerplate code and you might also need to implement threading.  
And that's why I'm so happy that Channels is here and will be part of Django core with version 1.10.

Here is a quick run down of the features I like the most in no particular order:

1. User sessions and [authentication](http://channels.readthedocs.org/en/latest/getting-started.html#authentication) not different to plain old HTTP-Django:
> we don't want a solution where clients just ask for the right channels, as anyone could change the code and just put in private channel names

* Normal usage of [models](http://channels.readthedocs.org/en/latest/getting-started.html#models) and the Django ORM.

* Multi-cast messaging with the [Groups](http://channels.readthedocs.org/en/latest/getting-started.html#groups) feature – aka the beloved multi user chat example.


* Easy [routing](http://channels.readthedocs.org/en/latest/getting-started.html#routing) of Channels.

* Multiple [channel backends](http://channels.readthedocs.org/en/latest/deploying.html#setting-up-a-channel-backend) with Redis as the default.  
This allows us to use an In-memory layer in development and Redis in production.

* Channels can be replacements for [background tasks](http://channels.readthedocs.org/en/latest/inshort.html?highlight=Background%20tasks#what-is-channels). 

Make sure, you read the [concepts](http://channels.readthedocs.org/en/latest/concepts.html) page if you'd like to know more about Channels. [Channels is available as an Django application](https://pypi.python.org/pypi/channels) that works with Django >=1.8. With Django 1.10 Channels will be included directly.

Some more in depth posts about Channels:

* [Laying the Channels Groundwork](https://www.aeracode.org/2016/1/3/laying-channels-groundwork/)
* [Finally, Real-Time Django Is Here: Get Started with Django Channels](https://blog.heroku.com/archives/2016/3/17/in_deep_with_django_channels_the_future_of_real_time_apps_in_django)
* [Quick introduction to Django Channels](http://www.machinalis.com/blog/introduction-to-django-channels/)