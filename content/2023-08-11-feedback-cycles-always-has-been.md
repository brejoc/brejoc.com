---
layout: post
title: "It's all feedback cycles. Always will, always has been!"
date: '2023-08-11 00:00:00'
description: 'When working in teams the inevadible question starts popping up: What is actually agile? Is it Scrum, is it Kanban, is it story points, having retros, aso? Usually my answer is: "Whatever works for the team". That is of course cheating. But what is agile from my point of view?'
tags:
  - agile
  - engineering
  - programming
  - imho
---

When working in teams the inevadible question starts popping up: What's actually agile? Is it Scrum, is it Kanban, is it story points, having retros, aso? Usually my answer is: ”Whatever works for the team”. That is of course cheating, because a team could decide to do waterfall and that is very much not agile.

But what __is__ agile from my point of view? Let's take a look at the [agile manifesto](https://agilemanifesto.org/):

* **Individuals and interactions** over processes and tools  
* **Working software** over comprehensive documentation  
* **Customer collaboration** over contract negotiation  
* **Responding to change** over following a plan  

With the disclaimer: _That is, while there is value in the items on
the right, we value the items on the left more._

From my point of view it's all about feedback cycles. If you look at the left side, you see a summary for short and high quality feedback cycles:

1. Individuals and interactions. That's your colleagues. They should be the ones you're running your ideas by. That review your code, or that might do pair programming with you.

2. Working software. If you have running software, then you can show it to people. You can ask for feedback and see if it matches needs and expectations.

3. Customer collaboration. Well, those are the folks that should check the working software on a regular basis. The more often, the better! That's the feedback you need, because they need to work with that software in the end. Or even while you continue working on it. Their needs or perceptions might change, while you continue working on something. That's also a valuable feedback cycle for them.

4. Responding to change. If your iterations are several months long, how do you know that you are still on track? The longer you work without feedback, the longer you might be running into a completely wrong direction.

![It's just about feedback cycles? Always has been!](/content/images/2023/JustFeedbackCyles.jpg "It's just about feedback cyles!")
But how is this related to Scrum, Kanban, or anything of that kind? Well, it isn't necessarily. And that's why I'm always saying: ”Whatever works for the team”. But, and that's the catch, you should have short feedback cycles! No matter how you choose to organize, you need to have short feedback cycles. You should have feedback from your peers. You should have tests and reviews. Maybe even in the form of pair programming sessions. You should have CI/CD tooling that actually helps you. QA feedback that isn't an afterthought. Feedback from your customers or representatives of them on a regular basis. And that shouldn't be six months down the road. We want to get to a state of continuous and sustainable delivery!

Scrum or Kanban might be a good starting point for your team. But then you need to tweak the system to your needs. If something hinders your feedback cycles: Change it! The better you are with your feedback cycles, the better your incentive system for improvement becomes. And that's what is valued by customers in the end and enables faster or higher cash flow. And we all know that this keeps the lights on.
  
I wouldn't even stop here. From my point of view programming languages make teams more or less agile. Do you use a language with no [REPL](https://en.wikipedia.org/wiki/Read%E2%80%93eval%E2%80%93print_loop) and long compile times? That's less agile. The faster developers see results on the screen, the better it is. It's less attention draining and less error prone. You don't have fast tests you are developing against? That's less agile. I guess you see where I'm going with this. It's all feedback cycles. Always will, always has been!
