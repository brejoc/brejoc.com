---
layout: post
title: What does the IoT and your laptop have in common?
image: "/content/images/2015/05/IMG_20150521_120013-2-1-2.jpg"
date: '2015-03-07 22:17:39'
tags:
- linux
- iot
- proximity-cloud
---

Do you still remember [Cuttlefish](https://apps.ubuntu.com/cat/applications/cuttlefish/)? With this neat application your computer becomes aware of its surroundings and can react to a switch of the wifi network, a plugged in USB devices, aso. These would be the triggers.

What makes Cuttlefish so very useful are the reactions you can combine this with. E.g. I'm turning the phone in my home office off whenever I'm not at home. Makes sense, doesn't it? And there is plenty more you can do. But this should be something that is baked right into the OS itself. A local [IFTTT](https://ifttt.com/) if you will. Why shouldn't *your* computer be the control center for this? The internet of the things could start with your computer. We don't need extra gadgets for this!