---
title: "Every Day Carry/Camera for 2024"
date: 2024-02-08T15:46:06+01:00
draft: false
---

An EDC very much depends on the environment you are usually in. For me, this is going to look like this in 2024. 


![Multiple things arranged on a black notebook.](/content/images/2024/edc.jpg "")

The **OM System TG-7** is my perfect every day camera. This camera can take pretty much every situation I can think of. But admittedly that's mostly heavy rain here in Germany. And bumping it around of course. The image quality is so much better than what you get from a phone. And maybe even more importantly: It fit in the pockets of my pants! Not just the jacker!!! 🤯

I don't exactly know what it is about the **Parker pens**, but they just feel right in my hand. The size is just perfect and the blue ink from Parker has the right viscosity and flow on the paper. Yeah, it's weird, I know. 😅

For 2024 I've come back to a **dotted Moleskin notebook** after using Scribbles That Matter for a while. I like it more than I would have thought. But at the end of the day it's just paper to write on. ✍️

The **Timex T-80** got a lot of wrist time lately and I'm sure it will continue to do so. Very comfy, all steel und just stays out of the way. The only thing that I don't get is the "END." writing that appears when you activate Indiglo. Why, Timex? WHY? It only makes reading the time so much harder. Especially in the dark… when you need to read the time with Indiglo. I just don't get it. But apart from this the watch is really great.

Having a Phone is a no-brainer nowadays. I got the **iPhone 13 Mini** quite a while ago, because I'd like to have something that gets security updates even two years after it has been release. The size is perfect. I really don't want to have a larger phone again! Besides messaging and social media, the flashlight is the most used feature on this phone! 😄

Last but not least the **Oura ring**. I love wearing real watches. Not some fancy smartwatch that needs to be charged every two days. Also I can keep wearing it at night. That's not possible with something around my wrist. I tried it, it just doesn't work for me.