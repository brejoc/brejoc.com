---
layout: post
title: "You can't kill the Joker"
date: '2023-10-13 00:00:00'
description: "The Joker? What are we talking about here? It's not the comic book villain for sure. I'm talking about Joker the Clojure dialect and why it still has advantages over Babashka."
tags:
  - programming
  - lisp
  - clojure
  - imho
---

The Joker? What are we talking about here? It's not the comic book villain for sure. I'm talking about [Joker the Clojure dialect](https://joker-lang.org/): "Joker is a small interpreted dialect of Clojure written in Go. It is also a Clojure(Script) linter."

The aspect I'd like to focus on here is the interpreter. Unlinke Clojure, Joker does not require the JVM and has a fast startup time. A simple "Hello world" only takes a few milliseconds. And since Joker is written in Go, no additional dependencies are needed. Perfect for writing scripts. "But we've got [Babashka](https://babashka.org/) for that!", you might think. And you are right about that. Babashka is an even more complete Clojure implementation. That does not mean that Joker doesn't have some [batteries included](https://candid82.github.io/joker/). Also Joker has a… Joker up it's sleeve: It runs pretty much everywhere!

![Person with hoody and Guy Fawkes mask holding a joker card into the camera](/content/images/2023/joker.jpg "Photo by Ravi Roshan: https://www.pexels.com/photo/photo-of-a-person-holding-a-joker-card-7517589/")

While Babashka is tied to the platforms supported by GraalVM, Joker runs on all of the platforms you can compile Go on.  
For GraalVM that's: Linux (amd64/aarch64), macOS (amd64/aarch64), and Windows (amd64).

Go on the other hand supports a few more platforms:

```
$ go tool dist list
aix/ppc64
android/386
android/amd64
android/arm
android/arm64
darwin/amd64
darwin/arm64
dragonfly/amd64
freebsd/386
freebsd/amd64
freebsd/arm
freebsd/arm64
freebsd/riscv64
illumos/amd64
ios/amd64
ios/arm64
js/wasm
linux/386
linux/amd64
linux/arm
linux/arm64
linux/loong64
linux/mips
linux/mips64
linux/mips64le
linux/mipsle
linux/ppc64
linux/ppc64le
linux/riscv64
linux/s390x
netbsd/386
netbsd/amd64
netbsd/arm
netbsd/arm64
openbsd/386
openbsd/amd64
openbsd/arm
openbsd/arm64
plan9/386
plan9/amd64
plan9/arm
solaris/amd64
wasip1/wasm
windows/386
windows/amd64
windows/arm
windows/arm64
```

And that's exactly why we need Joker. It provides a consistent lisp scripting solution for many more architectures and operating systems.

PS: If you are more into Lua, go take a look at [Fennel](https://fennel-lang.org/). 
